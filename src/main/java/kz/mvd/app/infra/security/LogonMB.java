package kz.mvd.app.infra.security;

import com.github.adminfaces.template.config.AdminConfig;
import com.github.adminfaces.template.session.AdminSession;
import jakarta.ejb.EJB;
import jakarta.ejb.EJBException;
import jakarta.enterprise.context.SessionScoped;
import jakarta.enterprise.inject.Specializes;
import jakarta.inject.Inject;
import jakarta.inject.Named;
import kz.mvd.app.persistence.Admin;
import kz.mvd.app.service.AdminService;
import kz.mvd.app.util.Utils;
import lombok.Getter;
import lombok.Setter;
import org.omnifaces.util.Faces;

import jakarta.persistence.NoResultException;
import org.omnifaces.util.Messages;
import org.primefaces.PrimeFaces;

import java.io.IOException;
import java.io.Serializable;

/**
 * Created by rmpestano on 12/20/14.
 *
 * This is just a login example.
 *
 * AdminSession uses isLoggedIn to determine if user must be redirect to login page or not.
 * By default AdminSession isLoggedIn always resolves to true so it will not try to redirect user.
 *
 * If you already have your authorization mechanism which controls when user must be redirect to initial page or logon
 * you can skip this class.
 */
@Named
@SessionScoped
@Specializes
@Setter
@Getter
public class LogonMB extends AdminSession implements Serializable {

    private Admin currentUser;
    private String email;
    private String password;
    private boolean remember;
    @Inject
    private AdminConfig adminConfig;
    @EJB
    private AdminService service;

    public void login() throws IOException {
        try {
            currentUser = getService().login(email, password);
            Utils.addDetailMessage("Logged in successfully as <b>" + email + "</b>");
            Faces.getExternalContext().getFlash().setKeepMessages(true);
            Faces.redirect(adminConfig.getIndexPage());
        }catch (EJBException ex){
            Messages.addInfo(null,"Не верный пароль!");
            PrimeFaces.current().executeScript("hideBar();");

        }
    }

    @Override
    public boolean isLoggedIn() {
        return currentUser != null;
    }
}
