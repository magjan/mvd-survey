package kz.mvd.app.service;

import jakarta.ejb.EJBException;
import jakarta.ejb.Stateless;
import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
import kz.mvd.app.persistence.Admin;
import lombok.Getter;
import lombok.Setter;

import jakarta.persistence.NoResultException;

@Stateless
@Setter
@Getter
public class AdminService {

    @PersistenceContext(unitName = "mvd")
    private EntityManager entityManager;

    public Admin login(final String login, final String password) throws EJBException {
        return getEntityManager().createQuery("select a from Admin a where a.login = :login and a.password = :password", Admin.class)
                .setParameter("login", login)
                .setParameter("password", password)
                .getSingleResult();
    }

}