/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package kz.mvd.app.bean;

import kz.mvd.app.model.Car;
import kz.mvd.app.service.CarService;
import jakarta.inject.Inject;
import jakarta.inject.Named;
import kz.mvd.app.util.Utils;
import org.omnifaces.cdi.ViewScoped;
import org.omnifaces.util.Faces;

import java.io.Serializable;

import static kz.mvd.app.util.Utils.addDetailMessage;
import static com.github.adminfaces.template.util.Assert.has;

/**
 * @author rmpestano
 */
@Named
@ViewScoped
public class CarFormMB implements Serializable {
    private Integer id;
    private Car car;

    @Inject
    CarService carService;

    public void init() {
        if (Faces.isAjaxRequest()) {
            return;
        }
        if (has(id)) {
            car = carService.findById(id);
        } else {
            car = new Car();
        }
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Car getCar() {
        return car;
    }

    public void setCar(Car car) {
        this.car = car;
    }


    public void remove() {
        if (has(car) && has(car.getId())) {
            carService.remove(car);
            Utils.addDetailMessage("Car " + car.getModel()
                    + " removed successfully");
            Faces.getFlash().setKeepMessages(true);
            Faces.redirect("car-list.jsf");
        }
    }

    public void save() {
        String msg;
        if (car.getId() == null) {
            carService.insert(car);
            msg = "Car " + car.getModel() + " created successfully";
        } else {
            carService.update(car);
            msg = "Car " + car.getModel() + " updated successfully";
        }
        Utils.addDetailMessage(msg);
    }

    public void clear() {
        car = new Car();
        id = null;
    }

    public boolean isNew() {
        return car == null || car.getId() == null;
    }


}
