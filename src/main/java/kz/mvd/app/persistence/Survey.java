package kz.mvd.app.persistence;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.List;

@Entity
@Table(name = "surveys")
public class Survey {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "title_ru")
    private String titleRu;

    @Column(name = "title_kk")
    private String titleKk;

    @Column(name = "description_ru")
    private String descriptionRu;

    @Column(name = "description_kk")
    private String descriptionKk;

    @Column(name = "created_at")
    private LocalDateTime createdAt;

    @Column(name = "is_anonymous")
    private boolean isAnonymous;

    @OneToMany(mappedBy = "survey", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<Question> questions;

    // Constructors, getters, setters, and other properties
}
