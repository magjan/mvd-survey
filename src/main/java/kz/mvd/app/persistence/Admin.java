package kz.mvd.app.persistence;

import kz.mvd.app.persistence.common.BaseEntity;
import kz.mvd.app.persistence.dictionary.Organization;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import jakarta.persistence.*;

@Entity
@Table(name = "ADMIN")
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class Admin extends BaseEntity {
    @Column(name = "LOGIN")
    private String login;
    @Column(name = "PASSWORD")
    private String password;
    @JoinColumn(name = "ORGANIZATION_ID")
    @ManyToOne
    private Organization organization;
    @Override
    public String toString() {
        return login;
    }
}
