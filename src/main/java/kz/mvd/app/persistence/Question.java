package kz.mvd.app.persistence;

import javax.persistence.*;

@Entity
@Table(name = "questions")
public class Question {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "survey_id")
    private Survey survey;

    @Column(name = "question_text_ru")
    private String questionTextRu;

    @Column(name = "question_text_kk")
    private String questionTextKk;

    @Column(name = "question_type")
    private String questionType;

    // Constructors, getters, setters, and other properties
}
