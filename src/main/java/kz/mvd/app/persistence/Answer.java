package kz.mvd.app.persistence;

import javax.persistence.*;

@Entity
@Table(name = "answers")
public class Answer {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "question_id")
    private Question question;

    @Column(name = "answer_text_ru")
    private String answerTextRu;

    @Column(name = "answer_text_kk")
    private String answerTextKk;

    // Constructors, getters, setters, and other properties
}
