package kz.mvd.app.persistence.common;

import lombok.Getter;
import lombok.Setter;

import jakarta.persistence.Column;
import jakarta.persistence.MappedSuperclass;

@MappedSuperclass
@Setter
@Getter
public class Dictionary extends BaseEntity {
    @Column(name = "CODE")
    private String code;
    @Column(name = "RU")
    private String ru;
    @Column(name = "kk")
    private String kk;
}
