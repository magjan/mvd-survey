package kz.mvd.app.persistence.dictionary;

import kz.mvd.app.persistence.common.Dictionary;
import lombok.Getter;
import lombok.Setter;

import jakarta.persistence.Entity;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;

@Entity
@Table(name = "DIC_ORGANIZATION")
@Setter
@Getter
public class Organization extends Dictionary {
    @JoinColumn(name = "CITY_ID")
    @ManyToOne
    private City city;
}
