package kz.mvd.app.persistence.dictionary;

import kz.mvd.app.persistence.common.Dictionary;
import lombok.Getter;
import lombok.Setter;

import jakarta.persistence.Entity;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;

@Entity
@Table(name = "DIC_CITY")
@Setter
@Getter
public class City extends Dictionary {
    @JoinColumn(name = "DISTRICT_ID")
    @ManyToOne
    private District district;
}
