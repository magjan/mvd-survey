package kz.mvd.app.persistence.dictionary;


import jakarta.persistence.Entity;
import jakarta.persistence.Table;
import kz.mvd.app.persistence.common.Dictionary;

@Entity
@Table(name = "DIC_DISTRICT")
public class District extends Dictionary {}
