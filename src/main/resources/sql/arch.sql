CREATE TABLE admins (
                        id SERIAL PRIMARY KEY,
                        username VARCHAR(255) NOT NULL,
                        password VARCHAR(255) NOT NULL,
                        email VARCHAR(255) NOT NULL
);
-- Создание таблицы "Опросы"
CREATE TABLE surveys (
                         id SERIAL PRIMARY KEY,
                         title_ru VARCHAR(255) NOT NULL,
                         title_kk VARCHAR(255) NOT NULL,
                         description_ru TEXT,
                         description_kk TEXT,
                         created_at TIMESTAMPTZ DEFAULT NOW(),
                         is_anonymous BOOLEAN DEFAULT TRUE
);

-- Создание таблицы "Вопросы"
CREATE TABLE questions (
                           id SERIAL PRIMARY KEY,
                           survey_id INT,
                           question_text_ru TEXT,
                           question_text_kk TEXT,
                           question_type VARCHAR(50),
                           FOREIGN KEY (survey_id) REFERENCES surveys(id) ON DELETE CASCADE
);

-- Создание таблицы "Ответы"
CREATE TABLE answers (
                         id SERIAL PRIMARY KEY,
                         question_id INT,
                         answer_text_ru TEXT,
                         answer_text_kk TEXT,
                         FOREIGN KEY (question_id) REFERENCES questions(id) ON DELETE CASCADE
);

-- Создание таблицы "Ответы на опросы"
CREATE TABLE survey_answers (
                                id SERIAL PRIMARY KEY,
                                survey_id INT,
                                respondent_name VARCHAR(100),
                                respondent_email VARCHAR(100),
                                created_at TIMESTAMPTZ DEFAULT NOW(),
                                FOREIGN KEY (survey_id) REFERENCES surveys(id) ON DELETE CASCADE
);

-- Создание таблицы "Ответы на вопросы"
CREATE TABLE question_answers (
                                  id SERIAL PRIMARY KEY,
                                  survey_answer_id INT,
                                  question_id INT,
                                  answer TEXT,
                                  FOREIGN KEY (survey_answer_id) REFERENCES survey_answers(id) ON DELETE CASCADE,
                                  FOREIGN KEY (question_id) REFERENCES questions(id) ON DELETE CASCADE
);
